package com.test.berlinclock.actions;

import com.test.berlinclock.formatter.BerlinDateFormatter;
import com.test.berlinclock.model.BerlinClockDate;
import com.test.berlinclock.ui.BerlinClockView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CurrentDateAction extends DateAction {

    private final static String[] options = {"-c", "--current"};

    public CurrentDateAction() {
        super(options);
    }

    @Override
    public void run(String[] args) throws ParseException {
        DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        BerlinDateFormatter formatter = new BerlinDateFormatter();
        BerlinClockDate clockDate = formatter.formatDate(new Date());
        BerlinClockView clockView = new BerlinClockView(clockDate);
        clockView.print();
    }

    @Override
    public void printOption() {
        System.out.println("\t-c --current: Display the Berlin clock using the current system time");
    }
}
