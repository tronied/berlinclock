package com.test.berlinclock.actions;

import com.test.berlinclock.exceptions.BadArgumentsException;
import com.test.berlinclock.formatter.BerlinDateFormatter;
import com.test.berlinclock.model.BerlinClockDate;
import com.test.berlinclock.ui.BerlinClockView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class FixedDateAction extends DateAction {

    private final static String[] options = {"-s", "--set"};

    public FixedDateAction() {
        super(options);
    }

    @Override
    public void run(String[] args) throws ParseException, BadArgumentsException {
        if (args.length != 1) {
            throw new BadArgumentsException("Invalid option arguments: Expecting single argument using format HH:mm:ss e.g. 10:31:23");
        }
        DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        BerlinDateFormatter formatter = new BerlinDateFormatter();
        BerlinClockDate clockDate = formatter.formatDate(sdf.parse(args[0]));
        BerlinClockView clockView = new BerlinClockView(clockDate);
        clockView.print();
    }

    @Override
    public void printOption() {
        System.out.println("\t-s --set: Display the Berlin clock with a fixed date using the format HH:mm:ss e.g 10:31:23");
    }
}
