package com.test.berlinclock.actions;

import com.test.berlinclock.exceptions.BadArgumentsException;
import lombok.Data;

import java.text.ParseException;
import java.util.Arrays;

@Data
public abstract class DateAction {
    private String[] options;

    DateAction(String[] options) {
        this.options = options;
    }

    public boolean isMatch(String option) {
        return Arrays.stream(options)
                .filter(a -> a.equalsIgnoreCase(option)).count() > 0;
    }

    public abstract void run(String[] args) throws ParseException, BadArgumentsException;

    public abstract void printOption();
}
