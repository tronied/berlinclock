package com.test.berlinclock.exceptions;

public class BadArgumentsException extends Exception {
    public BadArgumentsException(String message) {
        super(message);
    }
}
