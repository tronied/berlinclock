package com.test.berlinclock.exceptions;

public class InvalidOptionException extends Exception {
    public InvalidOptionException(String message) {
        super(message);
    }
}
