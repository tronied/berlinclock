package com.test.berlinclock;

import com.test.berlinclock.ui.ConsoleColours;
import org.fusesource.jansi.AnsiConsole;

public class Application {
    public static void main(String[] args) {
        try {
            //Enable utility for multi-platform console colouring
            AnsiConsole.systemInstall();
            BerlinClock clock = new BerlinClock();
            clock.processArgs(args);
        } catch (Exception ex) {
            System.err.println(ConsoleColours.RED + "\nError - " + ex.getMessage() + ConsoleColours.RESET);
        } finally {
            AnsiConsole.systemUninstall();
        }
    }
}
