package com.test.berlinclock.formatter;

import com.test.berlinclock.model.BerlinClockDate;

import java.util.Calendar;
import java.util.Date;

public class BerlinDateFormatter implements DateFormatter<BerlinClockDate> {
    public BerlinClockDate formatDate(Date date) {
        BerlinClockDate result = new BerlinClockDate();
        Calendar calendar = Calendar.getInstance();
        if (calendar != null) {
            calendar.setTime(date);
            result.setEvenSeconds(calendar.get(Calendar.SECOND) % 2 == 0);
            result.setFiveHourBlocks(Math.abs(calendar.get(Calendar.HOUR_OF_DAY) / 5));
            result.setOneHourBlocks(calendar.get(Calendar.HOUR_OF_DAY) % 5);
            result.setFiveMinuteBlocks(Math.abs(calendar.get(Calendar.MINUTE) / 5));
            result.setOneMinuteBlocks(calendar.get(Calendar.MINUTE) % 5);
        }
        return result;
    }
}
