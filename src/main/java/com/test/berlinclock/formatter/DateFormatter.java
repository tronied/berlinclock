package com.test.berlinclock.formatter;

import java.util.Date;

interface DateFormatter<T> {
    T formatDate(Date date);
}
