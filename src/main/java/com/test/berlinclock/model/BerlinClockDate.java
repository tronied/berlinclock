package com.test.berlinclock.model;

import lombok.Data;

@Data
public class BerlinClockDate {
    private boolean evenSeconds;
    private int fiveHourBlocks;
    private int oneHourBlocks;
    private int fiveMinuteBlocks;
    private int oneMinuteBlocks;
}
