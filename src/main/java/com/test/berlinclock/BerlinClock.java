package com.test.berlinclock;

import com.test.berlinclock.actions.CurrentDateAction;
import com.test.berlinclock.actions.DateAction;
import com.test.berlinclock.actions.FixedDateAction;
import com.test.berlinclock.exceptions.BadArgumentsException;
import com.test.berlinclock.exceptions.InvalidOptionException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class BerlinClock {

    private List<DateAction> actions;

    BerlinClock() {
        initActions();
    }

    private void initActions() {
        actions = new ArrayList<>();
        actions.add(new CurrentDateAction());
        actions.add(new FixedDateAction());
    }

    void processArgs(String[] args) throws ParseException, InvalidOptionException, BadArgumentsException {
        if (args.length == 0) {
            System.out.println();
            System.out.println("Usage: java -jar <jar-file> [options]");
            System.out.println("Options: ");
            actions.forEach(DateAction::printOption);
            System.out.println();
        } else {
            for (int i = 0; i < args.length; i++) {
                if (args[i].startsWith("-")) {
                    String option = args[i];
                    Optional<DateAction> action = actions.stream()
                            .filter(a -> a.isMatch(option))
                            .findFirst();
                    List<String> foundArgs = new ArrayList<>();
                    while (++i < args.length) {
                        if (args[i].startsWith("-")) {
                            i--; //Retrace as we need to handle next option
                            break;
                        }
                        foundArgs.add(args[i]);
                    }
                    if (action.isPresent()) {
                        action.get().run(foundArgs.toArray(new String[0]));
                    } else {
                        throw new InvalidOptionException("Invalid option specified: " + option);
                    }
                }
            }
        }
    }
}
