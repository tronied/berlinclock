package com.test.berlinclock.ui;

import com.test.berlinclock.model.BerlinClockDate;

public class BerlinClockView implements ConsolePrintable {

    private static final String LAMP_SEPARATOR = "\t\t---------------------------------";

    private BerlinClockDate clockDate;

    public BerlinClockView(BerlinClockDate date) {
        this.clockDate = date;
    }

    @Override
    public void print() {
        System.out.println("\n\t\t             /------\\           ");
        System.out.println(String.format("\t\t             | %s |           ", getEvenSeconds()));
        System.out.println("\t\t             \\------/           ");
        System.out.println(LAMP_SEPARATOR);
        System.out.println(String.format("\t\t|  %s |", getFourBlocks(ConsoleColours.RED, clockDate.getFiveHourBlocks())));
        System.out.println(LAMP_SEPARATOR);
        System.out.println(String.format("\t\t|  %s |", getFourBlocks(ConsoleColours.RED, clockDate.getOneHourBlocks())));
        System.out.println(LAMP_SEPARATOR);
        System.out.println(String.format("\t\t|%s|", getFiveMinuteBlocks()));
        System.out.println(LAMP_SEPARATOR);
        System.out.println(String.format("\t\t|  %s |", getFourBlocks(ConsoleColours.YELLOW, clockDate.getOneMinuteBlocks())));
        System.out.println("\t\t---------------------------------\n");
    }

    private String getEvenSeconds() {
        return clockDate.isEvenSeconds() ? ConsoleColours.YELLOW + "XXXX" + ConsoleColours.RESET: "    ";
    }

    private String getFourBlocks(String colour, int timeFactor) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1;i < 5;i++) {
            boolean litLamp = (i <= timeFactor);
            sb.append(colour + ((litLamp) ? "XXXX" : "    ") + ConsoleColours.RESET + ((i < 4) ? " || " : ""));
        }
        return sb.toString();
    }

    private String getFiveMinuteBlocks() {
        StringBuilder sb = new StringBuilder();
        for (int i = 1;i < 12;i++) {
            boolean litLamp = (i <= clockDate.getFiveMinuteBlocks());
            String colour = (i % 3 == 0) ? ConsoleColours.RED : ConsoleColours.YELLOW;
            sb.append(colour + ((litLamp) ? "X" : " ") + ConsoleColours.RESET + ((i < 11) ? "||" : ""));
        }
        return sb.toString();
    }
}
