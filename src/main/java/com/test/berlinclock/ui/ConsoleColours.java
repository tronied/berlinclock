package com.test.berlinclock.ui;

public class ConsoleColours {
    public static final String RESET = "\033[0m";

    public static final String RED = "\033[0;31m";
    public static final String YELLOW = "\033[0;33m";
}
