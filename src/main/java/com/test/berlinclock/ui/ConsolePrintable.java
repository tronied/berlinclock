package com.test.berlinclock.ui;

public interface ConsolePrintable {
    void print();
}
