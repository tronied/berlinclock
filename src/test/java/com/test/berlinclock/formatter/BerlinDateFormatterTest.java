package com.test.berlinclock.formatter;

import com.test.berlinclock.model.BerlinClockDate;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class BerlinDateFormatterTest {

    private BerlinDateFormatter dateFormatter;

    @Before
    public void setUp() {
        dateFormatter = new BerlinDateFormatter();
    }

    @Test
    public void testProvidedExample() throws ParseException {
        Date date = getDateFromString("10:31:24");
        BerlinClockDate clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 2, 0, 6, 1);
    }

    @Test
    public void testMaxDate() throws ParseException {
        Date date = getDateFromString("23:59:59");
        BerlinClockDate clockDate = dateFormatter.formatDate(date);
        /* With the clock showing the maximum possible value, every lamp except the final in the one hour blocks should
         * be illuminated. It is unknown whether the actual german clock briefly illuminates the final lamp before
         * resetting to zero, but in our case the final lamp will never be lit as it will never reach 24. The next test
         * will verify what happens if 24:00:00 is passed. */
        assertDateMatch(clockDate, false, 4, 3, 11, 4);
    }

    @Test
    public void testMaxMinDate() throws ParseException {
        Date date = getDateFromString("24:00:00");
        BerlinClockDate clockDate = dateFormatter.formatDate(date);
        /* 24:00:00 is interpreted as midnight which is the same as 00:00:00 and therefore we'll expect all of the
         * lamps to be extinguished. */
        assertDateMatch(clockDate, true, 0, 0, 0, 0);
    }

    @Test
    public void testMinDate() throws ParseException {
        Date date = getDateFromString("00:00:00");
        BerlinClockDate clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 0, 0, 0, 0);
    }

    @Test
    public void testOutOfBoundsDate() throws ParseException {
        //The date rolls over, so this should be 14:23
        Date date = getDateFromString("36:23:23");
        BerlinClockDate clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, false, 2, 2, 4, 3);
    }

    @Test
    public void testFiveHourIntervals() throws ParseException {
        Date date = getDateFromString("05:00:00");
        BerlinClockDate clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 1, 0, 0, 0);

        date = getDateFromString("10:00:00");
        clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 2, 0, 0, 0);

        date = getDateFromString("20:00:00");
        clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 4, 0, 0, 0);
    }

    @Test
    public void testOneHourIntervals() throws ParseException {
        Date date = getDateFromString("02:00:00");
        BerlinClockDate clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 0, 2, 0, 0);

        date = getDateFromString("04:00:00");
        clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 0, 4, 0, 0);

        date = getDateFromString("06:00:00");
        clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 1, 1, 0, 0);
    }

    @Test
    public void testFiveMinuteIntervals() throws ParseException {
        Date date = getDateFromString("00:30:00");
        BerlinClockDate clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 0, 0, 6, 0);

        date = getDateFromString("00:45:00");
        clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 0, 0, 9, 0);

        date = getDateFromString("00:55:00");
        clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 0, 0, 11, 0);
    }

    @Test
    public void testOneMinuteIntervals() throws ParseException {
        Date date = getDateFromString("00:01:00");
        BerlinClockDate clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 0, 0, 0, 1);

        date = getDateFromString("00:04:00");
        clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 0, 0, 0, 4);

        date = getDateFromString("00:05:00");
        clockDate = dateFormatter.formatDate(date);
        assertDateMatch(clockDate, true, 0, 0, 1, 0);
    }

    private void assertDateMatch(BerlinClockDate clockDate, boolean evenSeconds, int fiveHourBlocks, int oneHourBlocks,
                                int fiveMinuteBlocks, int oneMinuteBlocks) {
        assertEquals(clockDate.isEvenSeconds(), evenSeconds);
        assertEquals(clockDate.getFiveHourBlocks(), fiveHourBlocks);
        assertEquals(clockDate.getOneHourBlocks(), oneHourBlocks);
        assertEquals(clockDate.getFiveMinuteBlocks(), fiveMinuteBlocks);
        assertEquals(clockDate.getOneMinuteBlocks(), oneMinuteBlocks);
    }

    private Date getDateFromString(String date) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        return formatter.parse(date);
    }
}
