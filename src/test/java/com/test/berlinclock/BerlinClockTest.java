package com.test.berlinclock;

import com.test.berlinclock.exceptions.BadArgumentsException;
import com.test.berlinclock.exceptions.InvalidOptionException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Performs end-to-end integration tests to verify the relevant exceptions and results are returned. The method
 * of verifying whether the correct date value is printed is difficult however due to the ASCII formatting. As such,
 * a best guess of the number of 'X' characters is used. For better accuracy, see BerlinDateFormatterTest.
 */
public class BerlinClockTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private BerlinClock berlinClock;

    @Before
    public void setUp() {
        berlinClock = new BerlinClock();
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void testCurrentTimeSmallID() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("-c"));
        assertTrue(hasDrawnClock(outContent.toString()));
    }

    @Test
    public void testCurrentTimeFullID() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("--current"));
        assertTrue(hasDrawnClock(outContent.toString()));
    }

    @Test
    public void testSpecifyTimeSmallID() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("-s", "10:31:29"));
        //Verify number of X's in the resulting output
        assertEquals(countInstances(outContent.toString(), "X"), 18);
    }

    @Test
    public void testSpecifyTimeFullID() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("--set", "10:31:29"));
        //Verify number of X's in the resulting output
        assertEquals(countInstances(outContent.toString(), "X"), 18);
    }

    @Test(expected = InvalidOptionException.class)
    public void testInvalidArgumentSmall() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("-u"));
    }

    @Test(expected = InvalidOptionException.class)
    public void testInvalidArgumentLarge() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("--unknown"));
    }

    @Test(expected = BadArgumentsException.class)
    public void testSetDateNoArguments() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("-s"));
    }

    @Test(expected = BadArgumentsException.class)
    public void testSetDateTooManyArguments() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("-s", "10:31:23", "12:19:41"));
    }

    @Test(expected = ParseException.class)
    public void tesInvalidDateCharacter() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("-s", "10:£1:23"));
    }

    @Test
    public void testMaxDateUnevenSeconds() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("--set", "23:59:59"));
        //Verify number of X's in the resulting output
        assertEquals(countInstances(outContent.toString(), "X"), 55);
    }

    @Test
    public void testMaxDateEvenSeconds() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("--set", "23:59:58"));
        //Verify number of X's in the resulting output
        assertEquals(countInstances(outContent.toString(), "X"), 59);
    }

    @Test
    public void testMinDateUnevenSeconds() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions("--set", "24:00:01"));
        //Verify number of X's in the resulting output
        assertEquals(countInstances(outContent.toString(), "X"), 0);
    }

    @Test
    public void testNoArgs() throws ParseException, BadArgumentsException, InvalidOptionException {
        berlinClock.processArgs(createOptions());
        //Verify number of X's in the resulting output
        assertTrue(outContent.toString().trim().startsWith("Usage:"));
    }

    private String[] createOptions(String...args) {
        return args;
    }

    private int countInstances(String value, String subset) {
        int result = 0, offset = 0, index = 0;
        while ((offset = value.indexOf(subset, index)) != -1) {
            index = offset + subset.length();
            result++;
        }
        return result;
    }

    private boolean hasDrawnClock(String output) {
        return countInstances(outContent.toString(), "\n") == 14;
    }

    @After
    public void restore() {
        System.setOut(System.out);
        System.setErr(System.err);
    }
}
