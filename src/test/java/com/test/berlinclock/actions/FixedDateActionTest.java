package com.test.berlinclock.actions;

import com.test.berlinclock.exceptions.BadArgumentsException;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

import static junit.framework.TestCase.assertTrue;

public class FixedDateActionTest {
    private FixedDateAction fixedDateAction;
    private String[] badDateValue = {"18:£4:x3"};

    @Before
    public void setUp() {
        fixedDateAction = new FixedDateAction();
    }

    @Test
    public void testOptionShortMatch() {
        assertTrue(fixedDateAction.isMatch("-s"));
    }

    @Test
    public void testOptionLongMatch() {
        assertTrue(fixedDateAction.isMatch("--set"));
    }

    @Test(expected = BadArgumentsException.class)
    public void testInvalidArguments() throws ParseException, BadArgumentsException {
        fixedDateAction.run(new String[0]);
    }

    @Test(expected = ParseException.class)
    public void testBadDateArgument() throws ParseException, BadArgumentsException {
        fixedDateAction.run(badDateValue);
    }
}
