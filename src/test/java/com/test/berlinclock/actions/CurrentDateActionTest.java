package com.test.berlinclock.actions;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CurrentDateActionTest {
    private CurrentDateAction currentDateAction;

    @Before
    public void setUp() {
        currentDateAction = new CurrentDateAction();
    }

    @Test
    public void testOptionShortMatch() {
        assertTrue(currentDateAction.isMatch("-c"));
    }

    @Test
    public void testOptionLongMatch() {
        assertTrue(currentDateAction.isMatch("--current"));
    }
}
