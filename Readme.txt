Name: Berlin Clock Test
Author: Robert Meyer
Date: 19-06-2018

Foreword
========
This readme has been written in mind with someone who is not technically knowledgeable, therefore please
bear this in mind when reading if it seems overly simplistic. It may be more helpful to get the help
running the project by running it from a terminal without arguments (as described in the options section below).

Pre-requisites
==============
Ensure you have both Java 1.8 and Maven installed before attempting to compile and run the project.

Compiling / Tests
=================
Navigate to the root of the project folder and type:

    mvn clean install

It will compile the project and run the unit tests before creating the runnable jar which can be found in the
target directory.

Running the Project
===================
Type the following once the project has been built:

    java -jar <jar-file> [options]

For example, if you're still in the root directory you can type the following to show the clock using the current
system time.

    java -jar ./target/berlinclock-1.0.jar -c

The above example uses forward slashes for MacOS / Linux, but simply replace them with back-slashes if you're using
a variant of Windows.

Options
=======
There are two options available which are:

    -c or --current:    Displays the Berlin Clock using the current system time
    -s or --set:        Uses an argument using the format HH:mm:ss to display a specific time

More details on each of the application options can be seen by running the command and passing no parameters e.g.

    java -jar ./target/berlinclock-1.0.jar



